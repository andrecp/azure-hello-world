package main

import (
	"fmt"
	"log"
	"net/http"
)

func sayHello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello World!")
}

func handleRequests() {
	http.HandleFunc("/", sayHello)
	log.Fatal(http.ListenAndServe("0.0.0.0:8080", nil))
}

func main() {
	handleRequests()
}
