# azure hello world

A project in Azure creating a backend that returns "Hello World!" and auto scales based on CPU usage (1-3 nodes).

```bash
> curl azh-public-lb.westus2.cloudapp.azure.com
Hello World!%        
```

## Code organization

The root folder contains the Golang backend, the infrastructure folder contains the terraform script to spin up the Azure resources.

## Set up your environment

We're using terraform and azure on a Mac, follow the instructions on [terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli) and [azure](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli) for your OS.

You will also need an Azure account.

For the backend follow the instructions on [golang](https://golang.org/doc/install)

## The backend

We're using `Make` to automate the workflows in Golang:
* `make build` builds the app
* `make test` runs the test suite
* `make run` starts the application locally in port 8080

We have a gitlab pipeline set up so that everytime a commit makes to the `main` branch we generate a new binary, the latest version of the binary is pulled by the Azure infrastructure everytime a VM starts through cloudinit.

We only allow code to be merged to `main` if it passes the tests. We disabled push straight to main.

## The infrastructure

We're creating a couple of resources in Azure, in a high level:
* *Resource group* is where all of our resources are created inside
* *Public IP* used by our load balancer to be exposed to the internet
* *Load Balancer* route public internet traffic to our backend app VMs
* *Virtual Machine Scale Set* runs our app in a private network, auto scales based on CPU usage
* A network and a subnet, rules for network security and auto scaling

Log in to Azure so that terraform can access your tokens

```bash
> az login
```

To create the infrastructure go to the infrastructure folder and

```bash
> terraform init
> terraform apply
```

And to see it's all working

```bash
> curl azh-public-lb.westus2.cloudapp.azure.com
Hello World!%
```

Once you're done run `terraform destroy` so that you do not occurr costs.


### TODO

* Finish the metrics & alerting set up on probe
* Endpoint to kill the app
