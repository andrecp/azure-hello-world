# Configure the Azure provider
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">= 2.26"
    }
  }

  required_version = ">= 0.14.9"
}

provider "azurerm" {
  features {}
}

# Create a resource group for the azh test project
resource "azurerm_resource_group" "azh_test_rg" {
  name     = var.resource_group_name
  location = var.location
}

# Create a virtual network so that our VMs are in an internal network
resource "azurerm_virtual_network" "vnet" {
  name                = "azh_vnet"
  address_space       = ["10.0.0.0/16"]
  location            = var.location
  resource_group_name = var.resource_group_name
  depends_on          = [azurerm_resource_group.azh_test_rg]
}

# Create a subnet in the virtual network
resource "azurerm_subnet" "subnet" {
  name                 = "subnet"
  resource_group_name  = var.resource_group_name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = ["10.0.2.0/24"]
}

# Create a storage account so that we can log in to the VM via the Azure portal
resource "azurerm_storage_account" "azh_storage" {
  name                     = "azhstorage13314"
  resource_group_name      = var.resource_group_name
  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = "GRS"
  depends_on               = [azurerm_resource_group.azh_test_rg]
}

# Use a vm scale set for high availability
resource "azurerm_linux_virtual_machine_scale_set" "compute" {
  name                = "azh-vmss"
  resource_group_name = var.resource_group_name
  location            = var.location
  sku                 = "Standard_B1s"
  instances           = 2

  # Note that in a real production set up we would use a ssh key instead
  admin_username                  = "azh_admin"
  admin_password                  = "HTggnwjAVAy3qL8TVYG8xhuBz4FiHa8sC7p8kFC2f"
  disable_password_authentication = false
  custom_data                     = base64encode(data.local_file.cloudinit.content)

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  network_interface {
    name    = "eth0"
    primary = true

    ip_configuration {
      name                                   = "internal"
      primary                                = true
      subnet_id                              = azurerm_subnet.subnet.id
      load_balancer_backend_address_pool_ids = [azurerm_lb_backend_address_pool.azh_lb_pool.id]
    }

  }

  os_disk {
    storage_account_type = "Standard_LRS"
    caching              = "ReadWrite"
  }

  boot_diagnostics {
    storage_account_uri = azurerm_storage_account.azh_storage.primary_blob_endpoint
  }


  # Since these can change via auto-scaling outside of Terraform,
  # let's ignore any changes to the number of instances
  lifecycle {
    ignore_changes = [instances]
  }
}

# Our cloud init configuration to create the VM
data "local_file" "cloudinit" {
  filename = "${path.module}/cloudinit.conf"
}

# AutoScaler based on CPU utilization
resource "azurerm_monitor_autoscale_setting" "autoscaler" {
  name                = "autoscale-config"
  resource_group_name = var.resource_group_name
  location            = var.location
  target_resource_id  = azurerm_linux_virtual_machine_scale_set.compute.id

  profile {
    name = "AutoScale"

    capacity {
      default = 2
      minimum = 1
      maximum = 3
    }

    rule {
      metric_trigger {
        metric_name        = "Percentage CPU"
        metric_resource_id = azurerm_linux_virtual_machine_scale_set.compute.id
        time_grain         = "PT1M"
        statistic          = "Average"
        time_window        = "PT5M"
        time_aggregation   = "Average"
        operator           = "GreaterThan"
        threshold          = 75
      }

      scale_action {
        direction = "Increase"
        type      = "ChangeCount"
        value     = "1"
        cooldown  = "PT1M"
      }
    }

    rule {
      metric_trigger {
        metric_name        = "Percentage CPU"
        metric_resource_id = azurerm_linux_virtual_machine_scale_set.compute.id
        time_grain         = "PT1M"
        statistic          = "Average"
        time_window        = "PT5M"
        time_aggregation   = "Average"
        operator           = "LessThan"
        threshold          = 25
      }

      scale_action {
        direction = "Decrease"
        type      = "ChangeCount"
        value     = "1"
        cooldown  = "PT1M"
      }
    }
  }
}

# Create a public IP to be used by the load balancer
resource "azurerm_public_ip" "azh_public_ip" {
  name                = "PublicIPForLB"
  location            = var.location
  resource_group_name = var.resource_group_name
  allocation_method   = "Static"
  domain_name_label   = "azh-public-lb"
  depends_on          = [azurerm_resource_group.azh_test_rg]
  sku                 = "Standard"
}

# Use a load balancer to point to the vm scaleset
resource "azurerm_lb" "azh_loadbalancer" {
  name                = "azhLoadBalancer"
  location            = var.location
  resource_group_name = var.resource_group_name
  sku                 = "Standard"

  frontend_ip_configuration {
    name                       = "PublicIPAddress"
    public_ip_address_id       = azurerm_public_ip.azh_public_ip.id
    private_ip_address_version = "IPv4"
  }
}

# Configure address pool for the load balancer
resource "azurerm_lb_backend_address_pool" "azh_lb_pool" {
  resource_group_name = var.resource_group_name
  loadbalancer_id     = azurerm_lb.azh_loadbalancer.id
  name                = "BackEndAddressPool"
}

# Configure rule to get to the backend for the load balancer
resource "azurerm_lb_rule" "azh_lb_rule" {
  resource_group_name            = var.resource_group_name
  loadbalancer_id                = azurerm_lb.azh_loadbalancer.id
  name                           = "LBRule"
  protocol                       = "Tcp"
  frontend_port                  = 80
  backend_port                   = 8080
  frontend_ip_configuration_name = "PublicIPAddress"
  probe_id                       = azurerm_lb_probe.azh_lb_healthcheck.id
  backend_address_pool_id        = azurerm_lb_backend_address_pool.azh_lb_pool.id
}

# Configure a health check to detect if a VM is health and can serve traffic
resource "azurerm_lb_probe" "azh_lb_healthcheck" {
  resource_group_name = var.resource_group_name
  loadbalancer_id     = azurerm_lb.azh_loadbalancer.id
  name                = "http-probe"
  protocol            = "Http"
  request_path        = "/"
  port                = 8080
}

# Create a security rule to allow the internet to hit our load balancer
resource "azurerm_network_security_group" "example" {
  name                = "example-nsg"
  location            = var.location
  resource_group_name = var.resource_group_name

  depends_on = [azurerm_resource_group.azh_test_rg]

  security_rule {
    name                       = "allow_inbound"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

# Connect the security rule to the subnet
resource "azurerm_subnet_network_security_group_association" "network_security_association" {
  subnet_id                 = azurerm_subnet.subnet.id
  network_security_group_id = azurerm_network_security_group.example.id
}

# Set up monitoring
# TODO: Stopped here.

# resource "azurerm_monitor_action_group" "monitor_action_group" {
#   name                = "monitor_action_group"
#   resource_group_name = azurerm_resource_group.azh_test_rg.name

#   email_receiver {
#     name          = "sendtoadmin"
#     email_address = "admin@admin.com"
#   }
# }

# resource "azurerm_monitor_metric_alert" "example" {
#   name                = "healthcheck_failing"
#   resource_group_name = azurerm_resource_group.azh_test_rg.name
#   scopes              = [azurerm_storage_account.azh_storage.id]
#   description         = "Action will be triggered when HTTP request on port 8080 fails."

#   criteria {
#     metric_namespace = "Microsoft.Storage/storageAccounts"
#     metric_name      = "Transactions"
#     aggregation      = "Total"
#     operator         = "GreaterThan"
#     threshold        = 50

#     dimension {
#       name     = "ApiName"
#       operator = "Include"
#       values   = ["*"]
#     }
#   }

#   action {
#     action_group_id = azurerm_monitor_action_group.monitor_action_group.id
#   }
# }
