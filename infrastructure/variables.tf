variable "location" {
  type        = string
  description = "Which location from Azure to create the resources in."
}

variable "resource_group_name" {
  type        = string
  description = "The name of the resource group."
}
